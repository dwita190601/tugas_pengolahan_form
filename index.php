
<?php 

if (isset($_POST['submit'])) {
  $nama = $_POST['namasiswa'];
  $mapel = $_POST['mapelsiswa'];
  $nilai_tugas = $_POST ['nilaitugas'];
  $nilai_uts = $_POST ['nilaiuts'];
  $nilai_uas = $_POST ['nilaiuas'];

  $nilaitotal = ($_POST['nilaitugas'] * 0.15) + ($_POST['nilaiuts'] * 0.35) + ($_POST['nilaiuas'] * 0.50);


    if ( $nilaitotal >= 90 && $nilaitotal <= 100) {
      $gradenilai = "A";
    } elseif ( $nilaitotal > 70 && $nilaitotal < 90) {
      $gradenilai = "B";
    } elseif ( $nilaitotal > 50 && $nilaitotal <= 70) {
      $gradenilai = "C";
    } elseif ( $nilaitotal <= 50 )  {
      $gradenilai = "D";
    } else {
      $gradenilai = "E";
    }

}

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Perhitungan Nilai Siswa</title>
    <style>
       body{background-color: rgb(224, 235, 235) ;}
       h5{text-decoration: underline;}
    </style>
  </head>
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-3 p-3 ">
            <h4 align="center" >FORM NILAI SISWA</h4> <br>
                <form action="" method="POST">
                  <div class="mb-3">
                      <label for="name" class="form-label">Nama Siswa/Siswi</label>
                      <input type="text" name="namasiswa" class="form-control" id="name">
                  </div>
                  <div class="mb-3">
                      <label for="mapel" class="form-label">Mata Pelajaran</label>
                      <input type="text" name="mapelsiswa" class="form-control" id="mapel">
                  </div>
                  <div class="mb-3">
                      <label for="tugas" class="form-label">Nilai Tugas</label>
                      <input type="number" name="nilaitugas" class="form-control" id="tugas">
                  </div>
                  <div class="mb-3">
                      <label for="uts" class="form-label">Nilai UTS</label>
                      <input type="number" name="nilaiuts" class="form-control" id="uts">
                  </div>
                  <div class="mb-3">
                      <label for="uas" class="form-label">Nilai UAS</label>
                      <input type="number" name="nilaiuas" class="form-control" id="uas">
                  </div>
                  <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
        </div> 
        <?php if(isset($_POST['submit'])): ?>
        <div class="row justify-content-center">
            <div class="col-6 border border-primary mt-3 p-3 ">
              <div class="alert alert success">
                <h5 align="center">HASIL PENGOLAHAN FORM NILAI SISWA</h5><br>
                  Nama Siswa/Siswi  : <?php echo $nama ?> <br>
                  Mata Pelajaran    : <?php echo $mapel ?> <br>
                  Nilai Tugas       : <?php echo $nilai_tugas ?> <br>
                  Nilai UTS         : <?php echo $nilai_uts ?> <br>
                  Nilai UAS         : <?php echo $nilai_uas ?> <br>
                  Total Nilai Siswa : <?php echo $nilaitotal ?> <br>
                  Grade Nilai       : <?php echo $gradenilai ?> <br>
              </div>
            </div>
        </div>
      <?php endif; ?>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

  </body>
</html>